let firstNumber;
let secondNumber;
let userSign;

do {
    firstNumber = +prompt("enter the first number", 2);
    secondNumber = +prompt("enter the second number", 3);
    userSign = prompt("enter your sign: +, -, *, /", '+')
} while ((isNaN(firstNumber) === true) || (isNaN(secondNumber) === true))

function calcResult(firstNumb, secondNumb, sign) {
    switch (sign) {
        case "+" :
            return firstNumb + secondNumb;
        case "-" :
            return firstNumb - secondNumb;
        case "*" :
            return firstNumb * secondNumb;
        case "/" :
            return firstNumb / secondNumb;
    }
}

console.log('Result: ', calcResult(firstNumber, secondNumber, userSign));
